import numpy as np
import cv2
#from matplotlib import pyplot as plt

    
def seleccionarPuntos(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img, (x, y), 1, (0, 255, 0), 3)
        puntos.append([x, y])

def uniendoPuntos(puntos):
	cv2.line(img,tuple(puntos[0]),tuple(puntos[1]),(255,0,0),1)
	cv2.line(img,tuple(puntos[0]),tuple(puntos[2]),(255,0,0),1)
	cv2.line(img,tuple(puntos[2]),tuple(puntos[3]),(255,0,0),1)
	cv2.line(img,tuple(puntos[1]),tuple(puntos[3]),(255,0,0),1)

puntos = []
img = cv2.imread('camion.jpg')
aux = img.copy()
cv2.namedWindow('Imagen')
cv2.setMouseCallback('Imagen', seleccionarPuntos)

while True:

	if len(puntos) == 4:
		uniendoPuntos(puntos)
		pts1 = np.float32([puntos])
		pts2 = np.float32([[0,0], [480,0], [0,300], [480,300]])

		M = cv2.getPerspectiveTransform(pts1,pts2)
		homografia = cv2.warpPerspective(img, M, (480,300))

		cv2.imshow('Imagen en perspectiva', homografia)
	cv2.imshow('Imagen',img)
	
	k = cv2.waitKey(1) & 0xFF
	if k == ord('n'):
		imagen = aux.copy()
		puntos = []
		
	elif k == 27:
		break

cv2.waitKey(0)
cv2.destroyAllWindows()