
import cv2
import numpy as np

xi, yi, xf, yf = 0,0,0,0 # puntos del rectangulo
interruptor = False

def dibujar(event, x, y, flags, param):
    global xi, yi, xf, yf, interruptor, foto, recorte
    if event == cv2.EVENT_LBUTTONDOWN: #preciona botan izquerdo
        xi, yi = x, y #lo hemos capturado
        interruptor = False    
    if event == cv2.EVENT_LBUTTONUP:    #deja de precionarse boton izquierdo
        xf, yf = x, y
        interruptor = True
        recorte = foto[yi:yf, xi:xf,:]
        cv2.imwrite('recorte.png', recorte)


foto = cv2.imread('bob.jpg', 1)  #leo la imagen
cv2.namedWindow('imagen') #ventana que se abre la imagen
cv2.setMouseCallback('imagen', dibujar) #metodo para llamar la funcion


print("Al abrir la imagen presione 'g' para guardar, 'r' para generar un nuevo recorte o 'q' para finalizar")

while True:
    if interruptor == True:
        cv2.rectangle(foto, (xi, yi), (xf, yf), (255, 0, 0), 2)
    #se muestra la imagen que se hizo la lectura
    cv2.imshow('imagen', foto)

    k = cv2.waitKey(1)& 0xff

    if k == ord('g'):
        cv2.imwrite('NuevaImagen.jpg', recorte)
        break
    
    elif k == ord('r'):
        img = cv2.imread('bob.jpg')
        img = np.zeros((512,512,3), np.uint8)
        cv2.namedWindow('imagen')
        cv2.setMouseCallback('imagen', dibujar)

    elif (k == 27 or k == ord('q')):
        break

cv2.destroyAllWindows()