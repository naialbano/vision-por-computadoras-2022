# Frecuencia de imagen, también llamada frame rate o fotogramas por cuadro o por segundo.
# Un fotograma o frame es cada una de las imágenes que forman un vídeo
import cv2

cap = cv2.VideoCapture('mar.mp4')

fps = cap.get(cv2.CAP_PROP_FPS)
mspf = int((1/fps)*1000)

print(f"Cuadros por segundo: {fps}")
print(f"Milisegundos por cuadro: {mspf}")

while (cap.isOpened()):
    ret, frame = cap.read()
    if (ret == True):
        cv2.imshow("Mar", frame)
        if (cv2.waitKey(30) == ord('s')):
          
            break

cap.release()
cv2.destroyAllWindows()

