import cv2
import numpy as np

def inicializarDatos():
    global imagen1, puntos, imagen2

    puntos = []
    imagen1 = cv2.imread('pizarronNegro.jpg')
    imagen2 = cv2.imread('frase.jpg')

def seleccionarPuntos(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(imagen1, (x, y), 1, (0, 255, 0), 3)
        puntos.append([x, y])

def transformadaAfin(img, puntosDestino, ancho_final, alto_final):
    alto, ancho= img.shape[0], img.shape[1]
    puntosOrigen = np.float32([[0, 0], [ancho, 0], [0, alto]])

    M = cv2.getAffineTransform(puntosOrigen, puntosDestino)
    imgTransformadaAfin=cv2.warpAffine(imagen2, M, (ancho_final, alto_final))

    cv2.imwrite('imagenTransformadaAFin.jpg', imgTransformadaAfin)

    return imgTransformadaAfin

def incrustar(imagen1, imagenTransformada):
    umbral = [0, 0, 0]
    imagenIncrustada = imagenTransformada.copy()

    for x in range(imagenIncrustada.shape[1]):
        for y in range(imagenIncrustada.shape[0]):
            if (imagenIncrustada[y][x] == umbral).all():
                imagenIncrustada[y][x] = imagen1[y][x]

    return imagenIncrustada

inicializarDatos()
cv2.namedWindow('imagen')
cv2.setMouseCallback('imagen', seleccionarPuntos)

print("Al abrir la imagen, seleccione 3 puntos, presione 'g' para guardar, 'r' para generar nuevos puntos y 'q' para finalizar")

while (1):

    cv2.imshow('imagen', imagen1)
    k = cv2.waitKey(1) & 0xFF

    if k == ord('g'):
        if len(puntos) == 3:
            puntosDestino = np.float32([puntos])
            imagenTransformadaAfin=transformadaAfin(imagen2, puntosDestino, imagen1.shape[1], imagen1.shape[0])
            cv2.imwrite('imagenTrasnformadaAfin.jpg', imagenTransformadaAfin)

            imagenIncrustada=incrustar(imagen1, imagenTransformadaAfin)
            cv2.imshow('imagenFinal', imagenIncrustada)
            cv2.imwrite('imagenIncrustada.jpg', imagenIncrustada)
            break
    
    elif k == ord('r'):
        imagen1 = cv2.imread('pizarronNegro.jpg')
        puntos=[]
        continue

    elif (k == 27 or k == ord('q')):
        break

cv2.waitKey(0)
cv2.destroyAllWindows()
