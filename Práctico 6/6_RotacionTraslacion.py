import numpy as np
import cv2

def traslacionRotacion(imagen, tx, ty, angle):
    (h, w) = (imagen.shape[0], imagen.shape[1])
    M = np.float32([[1, 0, tx],
                    [0, 1, ty]])
    imagenTrasladada = cv2.warpAffine(imagen, M, (w, h))

    (h, w) = imagenTrasladada.shape[:2]
    M = cv2.getRotationMatrix2D((w/2, h/2), angle, 1.0)
    imagenFinal=cv2.warpAffine(imagenTrasladada, M, (w, h))

    return imagenFinal

imagen=cv2.imread('panda.jpg')
imagenFinal=traslacionRotacion(imagen, 50, 0, 10)

cv2.imshow('Imagen de entrada', imagen)
cv2.imshow('Imagen trasladada y rotada', imagenFinal)
cv2.waitKey(0)
cv2.destroyAllWindows()