import cv2

cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc('X','V','I','D')
out = cv2.VideoWriter('nuevoVideo.avi', fourcc, 20.0, (640,480))

while (cap.isOpened()):
    ret, frame = cap.read()
    if ret is True:
        out.write(frame)  
        cv2.imshow('frame', frame)
        width = cap.get(cv2.CAP_PROP_FRAME_WIDTH )
        height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT )
        mostrar = 'Ancho: {width}, Alto: {height}'
        print(mostrar.format(width=width, height=height))
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

cap.release()
out.release()
cv2.destroyAllWindows()

