print("--------------------------------------------------------")
print("Matriz principal: ")

lista = [[2,2,5,6],[0,3,7,4],[8,8,5,2],[1,5,6,1]] #defino lista
print('\n'.join([''.join(['{:4}'.format(item) for item in row]) 
      for row in lista]))

suma = 0
for i in range(len(lista)):
    for j in range(4):
        suma = suma + lista[i][j]

print("Tercera fila de la matriz: ")
print(lista[2]) #seleccionar array 2
lista[2]



print("Matriz diagonal en 0: ")
for i in range(len(lista)): #len me trae a primer nivel "lista" de la lista (las 4 filas serian)
    lista[i][i]=0
    
print('\n'.join([''.join(['{:4}'.format(item) for item in row]) 
      for row in lista]))

 #Suma realizada al comienzo para evitar cambio de números
print("Suma de todos los elementos de la matriz: ")
print(suma)

#Setear valores pares en o e impares en 1

for i in range(4):
    for j in range(4):
        if(lista[i][j]%2 == 0):
            lista[i][j]= 0
        else:
            lista[i][j] = 1

print("Matriz de 0 y 1: ")
print('\n'.join([''.join(['{:4}'.format(item) for item in row]) 
      for row in lista]))

